# -*- coding: utf-8 -*-
'''
Se hacen las importaciones de las librerias necesarias para el funcionamiento del cÃ³digo
'''
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QFileDialog, QWidget, QMessageBox
from matplotlib.figure import Figure
from PyQt5.uic import loadUi
from numpy import arange, sin, pi
#contenido para graficos de matplotlib
from matplotlib.backends. backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import scipy.io as sio
import numpy as np
import re

# clase con el lienzo (canvas=lienzo) para mostrar en la interfaz los graficos matplotlib, el canvas mete la grafica dentro de la interfaz
class MyGraphCanvas(FigureCanvas):
    #constructor
    def __init__(self, parent= None,width=8, height=6, dpi=150):
        #se crea un objeto figura
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        #el axes en donde va a estar mi grafico debe estar en mi figura
        self.axes = self.fig.add_subplot(111) #figura para visualizaciÃ³n de las seÃ±ales
        #self.axes2 = self.fig.add_subplot(212) #axes para la visualizaciÃ³n de los filtros
        #llamo al metodo para crear el primer grafico
        self.compute_initial_figure()
        #se inicializa la clase FigureCanvas con el objeto fig
        FigureCanvas.__init__(self,self.fig)
        
    def compute_initial_figure(self):
        '''
        Funcion que permite mostrar una senal inicial en la ventana cuando se abre
        '''
        t = arange(0.0, 10.0, 0.01) 
        s = 8*sin(0.5*pi*t)
        self.axes.plot(t,s,color = 'r') #grafico
        self.axes.grid() #activo la malla
        #designo nombres para ejes
        self.axes.set_xlabel("Muestras")
        self.axes.set_ylabel("Amplitud")
        
    def graficar_datos(self,datos):
        '''
        Funcion que permite graficar lo que sea que se ingrese para graficar
        '''
        self.axes.clear() #se limpian la grafica anterior
        self.axes.plot(datos) #Se ingresan los datos a graficar
        #Se grafican en el mismo plano los diferentes canales evitando superposiciÃ³n
        if len(datos.shape) == 1:
            self.axes.plot(datos)
        else:
            for c in range(datos.shape[0]):
                self.axes.plot(datos[c,:]+c*10)
        #se congiuran los ejes
        self.axes.set_xlabel("Muestras")
        self.axes.set_ylabel("Amplitud")
        #Se ordena que realice el grafico (o dibujo)
        self.axes.figure.canvas.draw()
        
    def graficar_espectro(self, time, datos, freqs, power):
        '''
        Funcion que permite graficar lo que sea que se ingrese para graficar
        '''
        self.axes.clear() #se limpian la grafica anterior
        self.axes.contourf(time,
                 freqs[(freqs >= 4) & (freqs <= 40)],
                 power[(freqs >= 4) & (freqs <= 40),],
                 20, # Especificar 20 divisiones en las escalas de color 
                 extend='both')
        self.axes.figure.canvas.draw()
    
    def graficar_canal(self,datos,canal):
        '''
        Funcion que permite que al seleccionar un canal, se grafique en la interfaz ese solo canal
        '''
        #se limpian la grafica anterior
        self.axes.clear()
        if len(datos.shape) == 1:
            self.axes.plot(datos)
        #Se grafica solo el canal selecionado
        else: self.axes.plot(datos[canal,:])
        self.axes.set_xlabel("Muestras")
        self.axes.set_ylabel("Amplitud")
        #Se ordena que realice el grafico (o dibujo)
        self.axes.figure.canvas.draw()

    def graficar_f(self, datos): ###Puede ser necesario eliminarla
        
        #Funcion que permite graficar las funciones en el axe 2, es decir las señales filtradas, analizadas
        #se limpia la grafica anterior
        self.axes2.clear() 
        #Se grafica la seÃ±al filtrada
        self.axes2.plot(datos)
        self.axes2.set_xlabel("Muestras")
        self.axes2.set_ylabel("Amplitud")
        #Se ordena que realice el grafico (o dibujo)
        self.axes2.figure.canvas.draw()
        
#%%
class InterfazGrafico(QMainWindow):
    '''
    Clase que permite crear las interfaces de los graficos
    '''
    #constructor
    def __init__(self):
        super(InterfazGrafico,self).__init__()
        #se carga el diseno de la ventana principal
        loadUi ('trabajo_2_senales.ui',self)
        #se llama la rutina donde configuramos la interfaz
        self.bandera = False
        self.setup()
        #se muestra la interfaz
        self.show()

    def setup(self):
        '''
        Funcion que permite organizar la interfaz en layouts, y aÃ±adir widgets apilados verticalmente
        '''
        layout = QVBoxLayout()
        #se aÃ±ade el organizador al campo grafico
        self.campo_grafico.setLayout(layout)
        #se crea un objeto para manejo de graficos
        self.__sc = MyGraphCanvas(self.campo_grafico, width=6, height=6, dpi=100)
        #se añade el campo de graficos
        layout.addWidget(self.__sc)
        #se configura la conexion de los botones cargar
        self.boton_cargar.clicked.connect(self.cargar_senal2)
        self.boton_obtener.clicked.connect(self.obtener_senal)
        #self.boton_multitaper.clicked.connect(self.Metodo_Multitaper)
        
        #Se configuran la conexiónn de los campos de tiempo inicial y final con sus funciones
        self.tiempoinicial.editingFinished.connect(self.tiempo_inicial)
        self.tiempofinal.editingFinished.connect(self.tiempo_final)
                
        #Se configura la conexion del spinbox que permite seleccionar el canal a filtrar
        #reacciona al cambiar el valor con las flechas del spinbox
        self.spinBox_canales.valueChanged.connect(self.canales) 
        #reacciona al cambiar el valor numericamente y dar enter
        self.spinBox_canales.editingFinished.connect(self.canales) 
              
        #Se desabilitan botones porque aun no se han cargado archivos con seÃ±ales a la interfaz
        self.boton_multitaper.setEnabled(False)
        self.boton_welch.setEnabled(False)
        self.boton_obtener.setEnabled(False)
        self.spinBox_canales.setEnabled(False)
        self.tiempoinicial.setEnabled(False)
        self.tiempofinal.setEnabled(False)
        
    def asignar_Controlador(self,controlador): #FunciÃ³n que asigna el controlador
        self.__coordinador=controlador
        
    def cargar_senal2(self):
        '''
        Funcon para abrir ventana emergente para cargar senales
        '''
        archivo_cargado, _ = QFileDialog.getOpenFileName(self, "Abrir senal","","Todos los archivos (*);;Archivos mat (*.mat)*")        
        if archivo_cargado != "" and archivo_cargado.endswith(".mat"):
            #Se carga la seÃ±al y se almacena
            data = sio.loadmat(archivo_cargado)
            claves = data.keys()
            self.ven = Cargar(self,claves,data)
            self.ven.show()
        else:
            self.campo_claves.setText("Senal seleccionada, no es archivo .mat, intente nuevamente")
            
    def cargar_senal(self, clave_ingresada,data):
        '''
        FunciÃ³n que permite cargar la seÃ±al que se adjunte diferenciando entre archivos .mat o .txt
        '''
        data = data[clave_ingresada]
        #Ingresa a este primer if si la señal tiene 2 dimensiones
        if len(data.shape) == 2:
            sensores,puntos=data.shape
            #Se vuelven continuos los datos
            senal_continua=np.reshape(data,(sensores,puntos),order="F")
            self.__coordinador.recibirDatosSenal(senal_continua)
        #Ingresa a este segundo si tiene 3 dimensiones
        elif len(data.shape) == 3:
            sensores,puntos,ensayos=data.shape
            #Se vuelven continuos los datos
            senal_continua=np.reshape(data,(sensores,puntos*ensayos),order="F")
            self.__coordinador.recibirDatosSenal(senal_continua)
        
        self.__x_min=0
        self.__x_max=puntos;            
        self.__sc.graficar_datos(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max))
        #Habilitar botones que permiten modificar seÃ±al
        self.boton_multitaper.setEnabled(True)
        self.boton_welch.setEnabled(True)
        self.boton_obtener.setEnabled(True)
        self.numero_canales.setEnabled(True)
        self.spinBox_canales.setEnabled(True)
        self.tiempoinicial.setEnabled(True)
        self.tiempofinal.setEnabled(True)       
    
        #Modifica el campo numero_canales mostrando el numero de canales que tiene la seÃ±al
        sen = str(sensores)
        self.numero_canales.setText(sen)
            
        #Restringe los valores mÃ¡ximos y mÃ­nimos que muestra el spinbox (evitar errores)
        self.spinBox_canales.setMaximum(sensores-1)
        self.spinBox_canales.setMinimum(0)
            
    def analizar_senal(self):
        '''
        Funcion para abrir ventana emergente para realizar analisis de la senal cargada
        '''
        
    def obtener_senal(self):
        '''
        Funcion para abrir ventana emergente para realizar analisis de la senal cargada con el metodo Wavelet
        '''
        self.spinBox_canales.setEnabled(False)
        self.tiempoinicial.setEnabled(False)
        self.tiempofinal.setEnabled(False)    
        self.ven = Obtener(self)
        self.ven.show()
        
    def canales(self):
        '''
        Funcion que permite seleccionar/modifical el canal que se visualiza de la seÃ±al
        '''
        canal = int(self.spinBox_canales.value()) #se conecta con el spinbox
        #se grafica la senal
        self.__sc.graficar_canal(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max),canal) 
    
    def tiempo_inicial(self):
        '''
        Funcion que permite modificar el tiempo incial de las seÃ±ales que se estan graficando 
        '''
        self.__x_min = int(self.tiempoinicial.text()) #se conecta con el campo de tiempo inicial
        #se grafica la senal
        self.__sc.graficar_datos(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max))
        #Si se esta visualizando una seÃ±al filtrada, se aplica el tiempo inicial a esta seÃ±al tambiÃ©n
        if self.bandera == True:
            self.__sc.graficar_canal(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max),int(self.spinBox_canales.value()))
            #se grafica la seÃ±al
            self.__sc.graficar_f(self.signal[self.__x_min:self.__x_max])

    def tiempo_final(self): 
        '''
        Funcion que permite modificar el tiempo final de las seÃ±ales que se estan visualizando
        '''
        self.__x_max = int(self.tiempofinal.text()) #se conecta con el campo de tiempo final
        #se grafica la seÃ±al
        self.__sc.graficar_datos(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max))
        #Si se esta visualizando una seÃ±al filtrada, se aplica el tiempo final a esta seÃ±al tambiÃ©n
        if self.bandera == True:
            self.__sc.graficar_canal(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max),int(self.spinBox_canales.value()))
            #se grafica la seÃ±al
            self.__sc.graficar_f(self.signal[self.__x_min:self.__x_max]) 

                           

    def update(self, signal):
        '''
        FunciÃ³n que permite graficar la seÃ±al que se esta filtrando
        '''
        self.__sc.graficar_canal(self.__coordinador.devolverDatosSenal(self.__x_min,self.__x_max),int(self.spinBox_canales.value()))
        self.__sc.graficar_f(signal[self.__x_min:self.__x_max])
        self.spinBox_canales.setEnabled(False) #desactiva el spinbox donde se elige el canal a visualizar
        #la bandera se utiliza en el codigo para determinar que se esta observando una seÃ±al filtrada
        self.bandera = True 
        self.signal = signal


    def guardar_senal(self, name):
        '''
        FunciÃ³n que permite almacenar la seÃ±al filtrada en un archivo png si el nombre que se asigna es diferente
        de "No"
        '''
        if name != "No":
            self.__sc.guardar_senal(name)
            #Se muestra una ventana emergente con el mensaje de que se guardo la seÃ±al
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Information)
            msg.setText(u"Imagen guardada con Ã©xito")
            msg.setWindowTitle("InformaciÃ³n")
            msg.show()
        else: 
            #Se muestra una ventana emergente con el mensaje de que NO se guardo la seÃ±al
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Information)
            msg.setText(u"No se guardÃ³ la imagen")
            msg.setWindowTitle("InformaciÃ³n")
            msg.show()
            
#%%
class Cargar(QWidget):
    '''
    Clase asociada a la ventana emergente que aparece cuando se presiona guardar, aquÃ­ se ingresa el nombre que se
    quiere dar al archivo
    '''
    #constructor
    def __init__(self, ventana, claves, data):
        QWidget.__init__(self)
        loadUi("senal_cargada.ui", self)
        self.win = ventana
        self.setup(claves,data)
        
    def asignar_Controlador(self,controlador): #FunciÃ³n que asigna el controlador
        self.__coordinador=controlador
        
    def setup(self,claves,data):
        '''
        Funcion que conecta los botones de la interfaz con sus respectivas funciones
        '''
        self.cl = str(claves)
        self.mostrar_claves.setText(self.cl)
        self.boton_guardar.clicked.connect(lambda:self.guardar(data))

    def guardar(self,data):
        '''
        Funcion que permite guardar la clave de la senal que se quiere cargar
        '''
        self.clave_ingresada = self.campo_clave.text()
        if self.clave_ingresada in data:
            self.win.cargar_senal(self.clave_ingresada, data)
            self.close()
        else: 
            msg = QMessageBox(self)
            msg.setIcon(QMessageBox.Information)
            msg.setText(u"No se ha ingresado una clave asociada a la senal cargada. Intentelo nuevamente")
            msg.setWindowTitle("Informacion")
            msg.show()
        
    
#%%
class Obtener(QWidget):
    '''
    Clase asociada a la ventana emergente que aparece cuando se presiona guardar, aquÃ­ se ingresa el nombre que se
    quiere dar al archivo
    '''
    #constructor
    def __init__(self, ventana):
        QWidget.__init__(self)
        loadUi("configurar.ui", self)
        self.win = ventana
        
    def asignar_Controlador(self,controlador): #FunciÃ³n que asigna el controlador
        self.__coordinador=controlador
        
    def setup(self):
        '''
        Funcion que conecta los botones de la interfaz con sus respectivas funciones
        '''
        self.boton_aplicar.clicked.connect(self.wavelet)
    
    def wavelet(self):
        fm = int(self.campo_fm.text())
        bfinicial = int(self.campo_bandai.text())
        bffinal = int(self.campo_bandaf.text())
        #tiempo, freq, power = self.__coordinador.CalcularWavelet(int(self.canales.value()))
        tiempo, freq, power = self.__coordinador.CalcularWavelet(int(self.canales.value()), fm, bfinicial, bffinal)
        self.win.graficar_espectro(tiempo, freq, power)
        #self.__sc.graficar_espectro(tiempo, freq, power)
        self.close()
        

       
        