# -*- coding: utf-8 -*-
import numpy as np

#Relacionado al Wavelet de la tarea 1
class Wavelet:
	def __init__(self, signal, tipo, lamb, ponderacion, level):
		self.__signal = signal
		self.tipo = tipo
		self.lamb = lamb
		self.ponderacion = ponderacion
		self.level = level

		self.ventanas = np.ones((4, 2), dtype=np.float64)
		self.ventanas[0,:] = [-1/np.sqrt(2), 1/np.sqrt(2)]
		self.ventanas[1,:] = [1/np.sqrt(2), 1/np.sqrt(2)]
		self.ventanas[2,:] = [1/np.sqrt(2), -1/np.sqrt(2)]
		self.ventanas[3,:] = [1/np.sqrt(2), 1/np.sqrt(2)]

	def descomposicion(self):
        #""" 
        #Función que se encarga de descomponer la señal a filtrar 
        #"""
		#Hacemos una copia de la señal
		signal_c = self.__signal.copy()
		#Lista para almacenar las partes de la descomposición
		almacenar = []
		#Str que se combierte en una línea de códico con eval
		#Es una matriz de ceros con dos filas y el numero de muestras que va tomando
		#la señal seccionada
		comando = "np.zeros((2,signal_c.shape[0]//2), dtype=np.float64)"
		#Ciclo para calcular el numero de aproximaciones y detalles según el orden ingresado
		for i in np.arange(0, self.level):
			#Si la dimensión de la matriz es impar.
			if signal_c.shape[0] % 2 != 0:
				#Agregamos un cero para que la dimensión sea par
				signal_c = np.append(signal_c, 0)
			#Convertimos el comando en una línea de código
			datos = eval(comando)
			#Calculamos la aproximacion
			apr = np.convolve(signal_c, self.ventanas[1,:], "full")[1::2]
			#Agregamos la aproximación a la primera fila del arreglo datos
			datos[0,:] = apr
			#Calculamos los detalles
			detalle = np.convolve(signal_c, self.ventanas[0,:], "full")[1::2]
			#Agregamos los detalles a la segunda fila del arreglo datos
			datos[1,:] = detalle
			#Agregamos las aproximaciones y detalles a la lista
			almacenar.append(datos)
			#Rescribimos la señal
			signal_c = apr
		#Retornamos los datos
		return almacenar

	def calcular_lambda(self, datos):
		'''
		Función encargada de calcular el lambda para el diseño del filtro
		'''
		#Array donde agregaremos todos los detalles
		x = np.array([])
		for i in np.arange(0,len(datos)):
			#Obtenemos los detalles almacenados
			detalle = datos[i][1]
			#Agregamos los detalles
			x = np.append(x, detalle)

		if self.lamb.upper() == 'U':
			lambd = np.sqrt(2*np.log(x.shape[0]))
		elif self.lamb.upper() == 'M':
			lambd = 0.3936+(0.1829*(np.log(np.log(x.shape[0]))/np.log(2))) 

		return lambd

	def calcular_ponderacion(self, lambd, datos):
		'''
		Funcion que calcula la matriz de umbrales para el filtro dependiendo
		de cada uno de los detalles generados, siendo el umbral el valor lambda
		de la señal de detalles multiplicado por el factor sigma
		'''
		#Arreglo que contiene los sigmas hallados para los detalles
		sigma = np.ones((1,len(datos)), dtype=np.float64)
		for i in np.arange(0,len(datos)):
			#Recuperamos los detalles
			detalle = datos[i][1]
			#Si se eligío la ponderacion ones
			if self.ponderacion == "ones":
				#Se debe devolver un vertor de unos
				break
			#Si usamos la ponderacion
			else: 
				#Se calcula el factor sigma
				sig = (np.median(np.absolute(detalle)))/0.6745
				#agregamos el valor hallado al arreglo de sigmas
				sigma[0,i] = sig
		#Retornamos el vector de umbrales
		return sigma*lambd

	def threshold(self, umbral, detalle):
		'''
		Funcion que se encarga de aplicar el filtro
		'''
		for i in np.arange(0,detalle.shape[0]):
			#Si escoge el Hard
			if self.tipo.upper() == "H":
				#Hacemos los datos menores al umbral cero
				if np.abs(detalle[i]) < umbral: detalle[i] = 0
			#Si se escoge el metod soft
			else:
				#Los valores por debajo del umbral se vulelven ceros
				if np.abs(detalle[i]) < umbral: detalle[i] = 0
				else: #De lo contrario
					#Obtenemos el sogno del dato
					sgn = detalle[i]/np.abs(detalle[i])
					#Hacemos la diferencia entre el valor y el umbral
					x = np.abs(detalle[i] - umbral)
					#Aplicamos la definición del método soft
					detalle[i] = sgn*x
		#Retornamos el nuevo vector de detalles
		return detalle 

	def reconstruir(self, datos):
		'''
		Funcion encargada de reconstruir la señal filtrada
		'''
		#Recorremos el vector de datos a la inversa
		for i in np.arange(0, self.level)[::-1]:
			#Obtenemos el el arreglo de aproximaciones y detalles
			signals = datos[i]
			#Array para almacenamiento de datos
			apr = np.squeeze(np.zeros((1,2*signals[0].shape[0]), dtype=np.float64))
			#Recuperamos los datos de aproximaciones y lo agregamos a los datos
			apr[0::2]= signals[0]
			#Llenamos con ceros las posiciones impares 
			apr[1::2]= 0
			#Array para almacenamiento de datos
			detalle = np.squeeze(np.zeros((1,2*signals[0].shape[0]), dtype=np.float64))
			#Recuperamos los datos de aproximaciones y lo agregamos a los datos
			detalle[0::2] = signals[1]
			#Llenamos con ceros las posiciones impares 
			detalle[1::2] = 0

			#Calculamos las aproximaciones
			aprox = np.convolve(apr, self.ventanas[3,:], "full")
			#Calculamos los detalles
			detalle = np.convolve(detalle, self.ventanas[2,:], "full")

			#Reconstruimos la señal
			sig = aprox+detalle
			#Si la dimensión de la nueva señal excede el vecto de datos original
			
			if sig.shape[0] > datos[i-1].shape[1]:
				if i != 0:
					#Eliminamos los datos de más
					sig = sig[0:datos[i-1].shape[1]]
					#Almacenamos la nueva señal
					datos[i-1][0] = sig
				else: 
					#Cuanso la señal está completamente reconstruida
					sig = sig[0:2*(datos[i].shape[1])]
					#Retornamos la señal reconstruida
					return sig

	def filtro(self):
		'''
		Implementacion de las funciones que se definieron arriba para construir el filtro
		'''
		#Descomponemos la señal en aproximaciones y detalles
		datos = self.descomposicion()
		#Calculamos el lambda de la señal
		lam = self.calcular_lambda(datos)

		#Calculamos el vector de umbrales
		umbral = self.calcular_ponderacion(lam, datos)[0,:]
		for i in np.arange(0,len(datos)):
			#Recuperamos los detalles
			detalle = datos[i][1]
			#Aplicamos el filtro
			datos[i][1] = self.threshold(umbral[i], detalle)
		#Reconstruimos la señal filtrada
		signal = self.reconstruir(datos)
		#Retornamos la señal filtrada
		return signal
    
#%% Manipulacion de la Biosenal
        
class Biosenal(object):
    def __init__(self,data=None):
        '''
        Función que asigna datos a las variables verificando que estos no queden vacíos
        '''
        if not data==None:
            self.asignarDatos(data)
        else:
            self.__data=np.asarray([])
            self.__canales=0
            self.__puntos=0
    def asignarDatos(self,data):
        ''' 
        Funcion que asigna datos que se introducen en la función a la variable data
        '''
        self.__data=data

    def devolver_segmento(self,x_min,x_max):
        '''
        Función que permite cargar las señales desde los archivos a la interfaz principal
        '''
        #Se evitan errores lógicos
        if x_min>=x_max:
        	if x_max != -1: return None
        #Se toman los valores que se necesiten de la señal cargada
        if len(self.__data.shape) == 1:
        	return self.__data[x_min:x_max]
        return self.__data[:,x_min:x_max]

    def escalar_senal(self, x_min, x_max, escalar):
        #Función que permite "aumentar" (ampliarla) o "disminuir" la señal
    	if len(self.__data.shape) > 1:
    		copia = self.__data[:,x_min:x_max].copy()
    	else:
    		copia = self.__data[x_min:x_max].copy()
    	return copia*escalar #retorna la señal escalada

#%% 
    
    def CalcularWavelet(self, canal, Vfs, Vban1, Vban2):
        #%% analisis usando wavelet continuo
        
        senal = self.__data[canal,:];
        import pywt #1.1.1
        
        #%%
        sampling_period =  1/Vfs
        Frequency_Band = [Vban1, Vban2] # Banda de frecuencia a analizar
        
        # Métodos de obtener las escalas para el Complex Morlet Wavelet  
        # Método 1:
        # Determinar las frecuencias respectivas para una escalas definidas
        scales = np.arange(1, Vfs)
        frequencies = pywt.scale2frequency('cmor', scales)/sampling_period
        # Extraer las escalas correspondientes a la banda de frecuencia a analizar
        scales = scales[(frequencies >= Frequency_Band[0]) & (frequencies <= Frequency_Band[1])] 
        
        N = senal.shape[0]
        
        #%%
        # Obtener el tiempo correspondiente a una epoca de la señal (en segundos)
        time_epoch = sampling_period*N
        
        # Analizar una epoca de un montaje (con las escalas del método 1)
        # Obtener el vector de tiempo adecuado para una epoca de un montaje de la señal
        time = np.arange(0, time_epoch, sampling_period)
        # Para la primera epoca del segundo montaje calcular la transformada continua de Wavelet, usando Complex Morlet Wavelet
        
        [coef, freqs] = pywt.cwt(senal, scales, 'cmor', sampling_period)
        # Calcular la potencia 
        power = (np.abs(coef)) ** 2
        
        return time, freqs, power
        