# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 01:04:04 2020

@author: NATY_
"""

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt

#C001R_EP_reposo señal de 3 dimensiones
#dataset_senales 
#promedios
#seneeg

data1 = sio.loadmat("seneeg.mat")
print("Los campos cargados son: " + str(data1.keys()))
clave = input("\nIngrese la clave donde se encuentran los datos que desea cargar: ")
data = data1[clave]
#Se vuelven continuos los datos
print(data.shape)
if len(data.shape) == 1:
    print("tiene 1 dimension" )
elif len(data.shape) == 2:
    print("tiene 2 dimensaiones")
    sensores,puntos = data.shape
    senal_continua=np.reshape(data,(sensores,puntos),order="F")
    plt.plot(sensores,puntos)
    plt.xlabel("Muestras")
    plt.ylabel("AMplitud")
    plt.show()
elif len(data.shape) == 3: 
    print("tiene 3 dimensaiones, y será redimensionada para poder graficarla")
    sensores,puntos,ensayos=data.shape
    senal_continua=np.reshape(data,(sensores,puntos*ensayos),order="F")
    plt.plot(senal_continua)
    plt.show()