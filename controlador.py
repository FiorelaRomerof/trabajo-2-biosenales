# -*- coding: utf-8 -*-
"""
Creación: Marzo de 2020
Autores: Fiorella Romero y Natalia Mercado
"""
from modelo import Biosenal
from modelo import Wavelet
from vista import InterfazGrafico
import sys
from PyQt5.QtWidgets import QApplication

class Principal(object):
    def __init__(self):        
        self.__app=QApplication(sys.argv)
        self.__mi_vista = InterfazGrafico()
        self.__mi_biosenal=Biosenal()
        self.__mi_controlador=Coordinador(self.__mi_vista,self.__mi_biosenal)
        self.__mi_vista.asignar_Controlador(self.__mi_controlador)
    def main(self):
        self.__mi_vista.show()
        sys.exit(self.__app.exec_())
    
class Coordinador(object):
    def __init__(self,vista,biosenal):
        self.__mi_vista=vista
        self.__mi_biosenal=biosenal
    def recibirDatosSenal(self,data):
        self.__mi_biosenal.asignarDatos(data)
    def devolverDatosSenal(self,x_min,x_max):
        return self.__mi_biosenal.devolver_segmento(x_min,x_max)
    def filtrar(self, signal, tipo, lam, pond, orden):
        filtro = Wavelet(signal, tipo, lam, pond, orden)
        return filtro.filtro()
    def CalcularWavelet(self, canal):
        return self.__mi_biosenal.CalcularWavelet(canal)

p=Principal()
p.main()